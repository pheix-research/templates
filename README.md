# Raku Template modules workspace

## HTML::Template vs Template::Toolkit

I've tried to compare **HTML::Template** and **Template::Toolkit**. [Here's](https://gitlab.com/pheix-research/templates/-/tree/master/ht-vs-tt) the result.

## XML based templating

I have hypothesis ({+&nbsp;PROVED!&nbsp;+}): the fastest way to render your page from the simple template is to use XML. The code is [here](https://gitlab.com/pheix-research/templates/-/tree/master/xml-tmpl).

Statistics for 100 iterations:

```bash
      xml render time:  0.1468574
 htmltmpl render time:  0.1475681
  regexpr render time:  0.3335471
template6 render time:  0.4282343
 mustache render time:  0.8034386
```

## Optimize HTML::Template based templating

The curious results are given by HTML::Template based templating [optimization](https://gitlab.com/pheix-research/templates/-/blob/b30b50d03f2650d385f7230181fb3899d159140b/reg-expr/lib/Pheix/View/TemplateH.rakumod). All we have to do is to [pre-parse](https://github.com/masak/html-template/blob/fa19261aa85b4e7dc50fe56de1e953fc821e6456/lib/HTML/Template.pm#L87) template with the grammar default [rules](https://github.com/masak/html-template/blob/master/lib/HTML/Template/Grammar.pm).

It's really important to parse the template once — cause the basic template has no often changes.

On other side we can trace the template changes by its size since the application start. So, if it's changes — repeat parsing process.

Statistics for 100 iterations **after optimization**:

```bash
 htmltmpl render time:  0.0025596 
      xml render time:  0.1474282
  regexpr render time:  0.3221913
template6 render time:  0.4282343
 mustache render time:  0.8003793
```

## License information

This is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Links and credits

[XML](https://github.com/raku-community-modules/raku-xml)

[HTML::Template](https://github.com/masak/html-template)

[Template::Toolkit](https://github.com/drforr/perl6-Template-Toolkit)

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
