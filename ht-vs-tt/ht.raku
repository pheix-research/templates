#!/usr/bin/env raku

use HTML::Template;

my $check;

my %params = (
    title => 'Hello Perl 6 world',
    authors => Array.new(
      { name => 'Ilya', id => 1 },
      { name => 'Moritz', id => 2 },
      { name => 'Lyle', id => 3 },
      { name => 'Carl', id =>4 },
      { name => 'Johan', id => 5 },
    ),
    clause => 0,
    error => 1,
);

my $ht = HTML::Template.from_file("tmpl/layout_2");

%params<clause> = 99;

$ht.with_params(%params);
print $ht.output;
