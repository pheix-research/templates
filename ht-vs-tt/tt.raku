#!/usr/bin/env raku

use lib './lib/perl6-Template-Toolkit/lib';

use Template::Toolkit;

my Str $tmpl;

my $tt = Template::Toolkit.new(OUTPUT => \($tmpl));
# my $tt = Template::Toolkit.new(OUTPUT => 'render.txt');
# my $tt = Template::Toolkit.new;

$tt.process( './tmpl/layout_1', { foo => '123', content => '345', items => ['first', 'last' ], userlist => [ { id => 1, name => 'kostas' }, { id => 2, name => 'Olga' } ], bool => 655361, val1 => 0, val2 => 20 } );

$tmpl.say;


