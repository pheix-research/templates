#!/usr/bin/env raku

use Template6;

my Str $workdir = IO::Path.new($*PROGRAM).dirname;
my Str $output  = q{};

my $tmplobj  = Template6.new;
my $template = slurp "$workdir/tmpl/layout_1.html";

$tmplobj.add-template: 'index', $template;

my %tmpl =
    pagetitle   => "This is the page title",
    bigbrojs    => "<script>doBigBroLook('/')</script>",
    content     => 'This is content',
    version     => '0.11.666',
    update_year => '2020',
    metadesc    => 'This is meta.description data',
    metakeys    => 'This is meta.keywords data',
    srvname     => 'This is meta.copyright data',
    timestamp   => now.Rat.Str
;

{
    my $start = now;

    $output = $tmplobj.process('index', :tmpl(%tmpl));

    ("processing time: " ~ (now - $start)).say;
}

spurt('./render.html', $output) if $workdir eq q{.};
