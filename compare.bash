#!/usr/bin/env bash

XML=
REGEXPR=
MUSTACHE=
HTMLT=
P6=
PWD=`pwd`
ITERATIONS=$1

if [ -z "$ITERATIONS" ] || ! [[ "$ITERATIONS" =~ ^[0-9]+$ ]]; then
    ITERATIONS=100
    echo "Fallback to default iterations count: ${ITERATIONS}"
else
    echo "User defined iterations count: ${ITERATIONS}"
fi

for (( i=1; i <= ${ITERATIONS}; i++ ))
do
    MY_XML=`bash -c "raku ${PWD}/xml-tmpl/html-2-xml.raku | grep -Eo '[0-9]+[.][0-9]+'"`

    if [ $? -ne 0 ]; then
        exit 1;
    fi

    MY_REGEXPR=`bash -c "raku -I${PWD}/reg-expr/lib ${PWD}/reg-expr/tmpl-regexpr.raku | grep -Eo '[0-9]+[.][0-9]+'"`

    if [ $? -ne 0 ]; then
        exit 1;
    fi

    MY_MUSTACHE=`bash -c "raku -I${PWD}/reg-expr/lib ${PWD}/reg-expr/tmpl-mustache.raku | grep -Eo '[0-9]+[.][0-9]+'"`

    if [ $? -ne 0 ]; then
        exit 1;
    fi

    MY_HTMLT=`bash -c "raku -I${PWD}/reg-expr/lib ${PWD}/reg-expr/tmpl-htmlt.raku | grep -Eo '[0-9]+[.][0-9]+'"`

    if [ $? -ne 0 ]; then
        exit 1;
    fi

    MY_P6=`bash -c "raku ${PWD}/tmpl6/tmpl-6.raku | grep -Eo '[0-9]+[.][0-9]+'"`

    if [ $? -ne 0 ]; then
        exit 1;
    fi

    XML=$(perl -e "print ${XML}+${MY_XML}")
    REGEXPR=$(perl -e "print ${REGEXPR}+${MY_REGEXPR}")
    MUSTACHE=$(perl -e "print ${MUSTACHE}+${MY_MUSTACHE}")
    HTMLT=$(perl -e "print ${HTMLT}+${MY_HTMLT}")
    P6=$(perl -e "print ${P6}+${MY_P6}")
done

echo "statistics for ${ITERATIONS} iters"
printf "%22s%2s%s\n" "htmltmpl render time:" "" $(perl -e "print sprintf '%.7f', ${HTMLT}/${ITERATIONS}")
printf "%22s%2s%s\n" "xml render time:" "" $(perl -e "print sprintf '%.7f', ${XML}/${ITERATIONS}")
printf "%22s%2s%s\n" "regexpr render time:" "" $(perl -e "print sprintf '%.7f', ${REGEXPR}/${ITERATIONS}")
printf "%22s%2s%s\n" "template6 render time:" "" $(perl -e "print sprintf '%.7f', ${P6}/${ITERATIONS}")
printf "%22s%2s%s\n" "mustache render time:" "" $(perl -e "print sprintf '%.7f', ${MUSTACHE}/${ITERATIONS}")
