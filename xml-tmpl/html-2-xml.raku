#!/usr/bin/env raku

use XML;

my Str $workdir = IO::Path.new($*PROGRAM).dirname;

my %report  =
    timestamps => 0,
    variables  => 0
;

my Str $output = q{};

my $xml = from-xml-file("$workdir/tmpl/layout_1.html");

$xml.doctype<type>  = 'html';
$xml.doctype<value> = q{};

my %tparams =
    title   => {
        name    => 'tmpl_pagetitle',
        new     => make-xml('title', "This is the page title"),
        existed => Nil,
        value   => q{}
    },
    bigbro  => {
        name    => 'tmpl_bigbrojs',
        new     => make-xml('script', "doBigBroLook('/')", :pheix-render-details<bigbro>),
        existed => Nil,
        value   => q{}
    },
    content => {
        name    => 'tmpl_content',
        new     => Nil,
        existed => Nil,
        value   => q{}
    },
    version => {
        name    => 'tmpl_version',
        new     => make-xml('span', '0.11.666'),
        existed => Nil,
        value   => q{}
    },
    year    => {
        name    => 'tmpl_update_year',
        new     => make-xml('span', '2020'),
        existed => Nil,
        value   => q{}
    },
    mdescr  => {
        name    => 'tmpl_metadesc',
        new     => Nil,
        existed => Nil,
        value   => 'This is meta.description data',
    },
    mkeys   => {
        name    => 'tmpl_metakeys',
        new     => Nil,
        existed => Nil,
        value   => 'This is meta.keywords data'
    },
    msrv    => {
        name    => 'tmpl_srvname',
        new     => Nil,
        existed => Nil,
        value   => 'This is meta.copyright data'
    }
;

for %tparams.keys -> $k {
    %tparams{$k}<existed> = $xml.root.elements(:TAG<pheixtemplate>, :variable(%tparams{$k}<name>), :RECURSE, :SINGLE);

    if !%tparams{$k}<existed> {
        %tparams{$k}<existed> = $xml.root.elements(:pheix-variable(%tparams{$k}<name>), :RECURSE, :SINGLE);
    }
}

my @timestampto = $xml.root.elements(:pheix-timestamp-to(* ~~ /<[a..z]>+/), :RECURSE);

{
    my $start = now;

    for (@timestampto) {
        my Str $attr = $_.attribs<pheix-timestamp-to>;

        if $attr {
            $_.set($attr, ($_.attribs{$attr} ~ q{?} ~ now.Rat));

            %report<timestamps>++;
        }
    }

    for %tparams.keys -> $k {

        if %tparams{$k}<name> eq 'tmpl_content' {
            %tparams{$k}<new> = make-xml('script', "loadAPI('page', '94a08da1fecbb6e8b46990538c7b50b2', 'GET', '/', '200', '')")
        }

        if %tparams{$k}<new> {
            %tparams{$k}<existed>.parent.replace(%tparams{$k}<existed>, %tparams{$k}<new>);

            %report<variables>++;
        }
        else {
            my Str $attr = %tparams{$k}<existed>.attribs<pheix-variable-to>;

            if $attr {
                %tparams{$k}<existed>.set($attr, %tparams{$k}<value>);

                %report<variables>++;
            }
        }
    }

    $output = $xml.Str;

    ("processing time: " ~ (now - $start)).say;
}

("added " ~ %report<timestamps> ~ " timestamps").say;
("replaced " ~ %report<variables>  ~ " variables").say;

spurt('./render.html', $output) if $workdir eq q{.};
