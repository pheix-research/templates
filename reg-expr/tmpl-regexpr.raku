#!/usr/bin/env raku

use Pheix::View::Template;

my Str $workdir = IO::Path.new($*PROGRAM).dirname;
my Str $output  = q{};

my $tmplobj = Pheix::View::Template.new(uitempl_indx => "$workdir/tmpl/layout_1.html");

my %tparams =
    tmpl_pagetitle   => "This is the page title",
    tmpl_bigbrojs    => "<script>doBigBroLook('/')</script>",
    tmpl_content     => q{},
    tmpl_version     => '0.11.666',
    tmpl_update_year => '2020',
    tmpl_metadesc    => 'This is meta.description data',
    tmpl_metakeys    => 'This is meta.keywords data',
    tmpl_srvname     => 'This is meta.copyright data',
    tmpl_timestamp   => now.Rat.Str
;

{
    my $start = now;

    $output = $tmplobj.render('index', %tparams);

    ("processing time: " ~ (now - $start)).say;
}

spurt('./render.html', $output) if $workdir eq q{.};
