unit class Pheix::View::Template;

has Str $.uitempl is default('{{{tmpl_debug}}}') is rw;
has Str $.uitempl_indx is default('./conf/config/index.html');
has Str $!indexdata =
    $!uitempl_indx && $!uitempl_indx.IO.e ??
        $!uitempl_indx.IO.slurp !!
            q{};

method render(Str $tmpl_type, %tmpl_vars) returns Str {
    my Str $rs;
    my Str $ht;

    given $tmpl_type {
        when 'debug' {
            $ht = $!uitempl;
        }
        default {
            $ht = $!indexdata;
        }
    }

    if $ht {
        $rs = self.fast_render(:template($ht), :vars(%tmpl_vars));
    }

    $rs // q{};
}

method reset_uitmpl(Str :$to) returns Pheix::View::Template {
    if $to {
        $!uitempl = $to;
    }
    else {
        $!uitempl = '{{{tmpl_debug}}}';
    }

    self;
}

method fast_render(Str :$template is copy, :%vars) returns Str {
    for %vars.keys -> $key {
        if $key ~~ /tmpl_timestamp/ {
            $template ~~ s:g/ \{\{\{?: <$key>: \}\}\}?: /%vars{$key}/;
        }
        else {
            $template ~~ s/ \{\{\{?: <$key>: \}\}\}?: /%vars{$key}/;
        }
    }

    $template;
}
