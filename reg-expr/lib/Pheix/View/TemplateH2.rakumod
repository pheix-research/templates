unit class Pheix::View::TemplateH2;

use HTML::Template;

has HTML::Template $!tmplobj = HTML::Template.new;
has Str $.uitempl is default('<TMPL_VAR tmpl_debug>') is rw;
has Str $.uitempl_indx is default('./conf/config/index.html');
has Str $!indexdata =
    $!uitempl_indx && $!uitempl_indx.IO.e ??
        $!uitempl_indx.IO.slurp !!
            q{};

method render(Str $tmpl_type, %tmpl_vars) returns Str {
    my Str $rs;
    my Str $ht;
    given $tmpl_type {
        when 'debug' {
            $ht = $!uitempl;
        }
        default {
            $ht = $!indexdata;
        }
    }
    if $ht {
        $rs = $!tmplobj.from_string($ht).with_params(%tmpl_vars).output;
    }
    $rs // q{};
}
