unit class Pheix::View::TemplateH;

use HTML::Template;

has Str $.uitempl is default('<TMPL_VAR tmpl_debug>') is rw;
has Str $.uitempl_indx is default('./conf/config/index.html');
has HTML::Template $!tobj is default(HTML::Template.new);

has $!indexparsed = $!uitempl_indx && $!uitempl_indx.IO.e ??
        $!tobj.from_string($!uitempl_indx.IO.slurp).parse !!
            Nil;

method render(Str $tmpl_type, %tmpl_vars) returns Str {
    my Str $rs;
    my $parsed;

    given $tmpl_type {
        when 'debug' {
            $parsed = $!tobj.from_string($!uitempl // q{}).parse // Nil;
        }
        default {
            $parsed = $!indexparsed;
        }
    }

    if $parsed {
        $rs = $!tobj.substitute($parsed, %tmpl_vars);
    }

    $rs // q{};
}
